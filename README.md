# @lpgroup/feathers-auth-service

Services and hooks to handle auth, user and organisations in feathersjs.

## Install

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/feathers-auth-service
```

## Example

```javascript
const auth = require("@lpgroup/feathers-auth-service");
```

## Contribute

See https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md

## License

MIT, see LICENSE.MD

## API

### `xxx`

#### `xxx(objSchema)`

```js
xxx(xxx);
```
