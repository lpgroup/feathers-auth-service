const debug = require("debug")("auth");
const errors = require("@feathersjs/errors");

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return function (context) {
    checkMethods(context);
    addRestrictionQuery(context);
    return context;
  };
};

function addRestrictionQuery(context) {
  const { path, method } = context;
  const { user, query } = context.params;

  const restrictions = user.permissions[path].query;
  if (restrictions) {
    for (var restriction of restrictions) {
      if (restriction.value.indexOf("*") === -1) {
        query[restriction.key] = {
          $in: restriction.value
        };
      }
    }
  } else {
    console.error(`Privilegies not configured for ${path} ${method} on user ${user._id}`);
    throw new errors.MethodNotAllowed(`Access denied ${path} ${method}`, {
      path,
      method
    });
  }
}

function checkMethods(context) {
  const { path, method } = context;
  const { user, superUser } = context.params;
  if (superUser) return;

  if (user && user.permissions[path] && user.permissions[path].methods) {
    const grants = user.permissions[path].methods;
    if (grants.indexOf("*") || grants.indexOf(method)) {
      debug(`Grants ${user.email} ${path} ${method} ${grants}`);
      return;
    }
  }

  debug(`Access denied ${user.email} ${path} ${method} ${JSON.stringify(user.permissions)}`);
  throw new errors.MethodNotAllowed(`Access denied ${path} ${method}`, {
    path,
    method
  });
}
