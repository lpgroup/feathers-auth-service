const { buildItemHook } = require("@lpgroup/feathers-utils");

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return buildItemHook((context, item) => {
    const superUser = context.params.superUser
      ? { _id: "superuser", organisationId: "superuser" }
      : undefined;

    const user = superUser || item.user || context.params.user;
    if (user) {
      item.changed = { by: user._id, at: Date.now() };
      if (!item.added) item.added = item.changed;
      if (!item.owner) item.owner = { organisation: { _id: user.organisationId } };
    }
  });
};
