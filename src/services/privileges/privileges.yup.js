const yup = require("@lpgroup/yup");

const requestSchema = {
  _id: yup.id(),
  token: yup.string(),
  permissions: yup.lazyObject({
    query: yup.arrayObject({ key: yup.string(), value: yup.array(yup.string()) }),
    methods: yup.array(yup.string())
  })
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner()
};

module.exports = yup.buildValidationSchema(requestSchema, dbSchema);
