const { Service } = require("@lpgroup/feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-utils");

exports.Privileges = class Privileges extends Service {
  constructor(options) {
    super(options);

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("privileges");
      database.createIndexThrowError("privileges", { token: 1 }, { unique: true });
    });
  }
};
