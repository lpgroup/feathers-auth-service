// Initializes the `privileges` service on path `/privileges`
const { Privileges } = require("./privileges.class");
const hooks = require("./privileges.hooks");
const schema = require("./privileges.yup");

module.exports = function (app) {
  const options = {
    id: "token",
    paginate: app.get("paginate"),
    schema
  };

  // Initialize our service with any options it requires
  app.use("/privileges", new Privileges(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("privileges");

  service.hooks(hooks);
};
