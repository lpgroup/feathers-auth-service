const yup = require("@lpgroup/yup");

//FIX need to make a grant service. instead of path user for permission.
const requestSchema = {
  _id: yup.id(),
  organisationId: yup.string().required("Välj ett företag"),
  email: yup.email().required("Skriv in din email"),
  password: yup.string().required("Skriv in ett lösenord"),
  firstName: yup.string().required("Skriv in förnamnet"),
  lastName: yup.string().required("Skriv in efternamnet"),
  phone: yup.phone().required(),
  permission: yup.array(yup.string())
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner()
};

module.exports = yup.buildValidationSchema(requestSchema, dbSchema);
