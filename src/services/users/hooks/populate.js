const { mergeWith, union } = require("lodash");

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async (context) => {
    const { app, method, result, params } = context;
    const users = method === "find" ? result.data : [result];

    await Promise.all(
      users.map(async (user) => {
        const newParams = {
          ...params,
          provider: "",
          query: {
            token: {
              $in: user.permission
            }
          }
        };

        // get privileges for the user.
        const privileges = await app.service("privileges").find(newParams);
        user.permissions = {};

        function customizer(objValue, srcValue) {
          if (Array.isArray(objValue)) {
            if (srcValue.indexOf("*") !== -1) return ["*"];
            return union(objValue, srcValue);
          }
        }

        privileges.data.forEach((row) => {
          mergeWith(user.permissions, row.permissions, customizer);
        });
      })
    );

    return context;
  };
};
