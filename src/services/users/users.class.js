const { Service } = require("@lpgroup/feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-utils");

exports.Users = class Users extends Service {
  constructor(options) {
    super(options);
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("users");
      database.createIndexThrowError("users", { email: 1 }, { unique: true });
    });
  }
};
