const debug = require("debug")("auth");
const { hashPassword, protect } = require("@feathersjs/authentication-local").hooks;
const { iff, isProvider } = require("feathers-hooks-common");
const { authenticate } = require("@feathersjs/authentication").hooks;
const { patchData, loadData } = require("@lpgroup/feathers-mongodb-hooks").hooks;
const { validateRequest, validateDatabase } = require("@lpgroup/yup");
const { url } = require("@lpgroup/feathers-utils").hooks;
const changed = require("../../hooks/changed");
const checkPermissions = require("../../hooks/check-permissions");
const setOwner = require("./hooks/set-owner");
const populate = require("./hooks/populate");

module.exports = {
  before: {
    // TODO: Så att superuser kan kolla /users
    //       iffprovider extern så checkGrants, om det är internt så det alltid vara ok
    all: [authenticate("jwt"), iff(isProvider("external"), checkPermissions())],
    find: [],
    get: [],
    create: [
      validateRequest(),
      setOwner(),
      hashPassword("password"),
      changed(),
      validateDatabase()
    ],
    update: [
      validateRequest(),
      setOwner(),
      loadData(),
      hashPassword("password"),
      changed(),
      validateDatabase()
    ],
    patch: [
      validateRequest(),
      setOwner(),
      patchData(),
      hashPassword("password"),
      changed(),
      validateDatabase()
    ],
    remove: []
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      url({ key: "alias" }),
      populate(),
      protect("password")
    ],
    find: [
      (context) => {
        if (context.result.total === 0) {
          debug(`Login Fail ${context.params.query.email}`);
        } else {
          debug(`Login Success ${context.result.data[0].email}`);
        }
      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [
      (context) => {
        debug(`Invalid user/JWT ${context.id}`);
      }
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
