const { Service } = require("@lpgroup/feathers-mongodb");
const { onPluginReady } = require("@lpgroup/feathers-utils");

exports.Organisations = class Organisations extends Service {
  constructor(options) {
    super(options);

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("organisations");
      database.createIndexThrowError("organisations", { alias: 1 }, { unique: true });
    });
  }
};
