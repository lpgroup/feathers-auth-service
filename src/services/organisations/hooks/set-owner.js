//TODO: Can we use the global hook?
// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async (context) => {
    const { data } = context;
    data.owner = { organisation: { _id: data._id } };
    return context;
  };
};
