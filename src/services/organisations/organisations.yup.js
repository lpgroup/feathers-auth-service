const yup = require("@lpgroup/yup");

const requestSchema = {
  _id: yup.uuid().required(),
  alias: yup.string().required(),
  name: yup.string().required(),
  organisationNumber: yup.string().required("Ange giltig organisationsnummer"),
  email: yup.string().email().required("Ange giltig E-Post"),
  phone: yup.phone("Ange ett giltigt telefonnummer"),
  website: yup.string().url(),
  contact: yup.object({
    firstName: yup.string(),
    lastName: yup.string(),
    phone: yup.phone("Ange ett giltigt telefonnummer"),
    email: yup.email().required("Ange giltig E-Post")
  }),
  postAddress: yup.object({
    street: yup.string(),
    city: yup.string(),
    zipCode: yup.string()
  })
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner()
};

module.exports = yup.buildValidationSchema(requestSchema, dbSchema);
module.exports.requestSchema = requestSchema;
