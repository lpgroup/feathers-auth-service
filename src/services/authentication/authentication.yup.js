const cy = require("@lpgroup/yup");

const requestSchema = {
  accessToken: cy.string(),
  email: cy.email(),
  password: cy.string(),
  strategy: cy.string().required()
};

module.exports = cy.buildValidationSchema(requestSchema);
