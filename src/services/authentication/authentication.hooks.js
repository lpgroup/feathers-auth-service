const { disallow } = require("feathers-hooks-common");
const { validateRequest } = require("@lpgroup/yup");
const { disableSync } = require("@lpgroup/feathers-utils").hooks;
const schema = require("./authentication.yup");

module.exports = {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [validateRequest({ schema })],
    update: [disallow()],
    patch: [disallow()],
    remove: []
  },

  after: {
    all: [disableSync()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
