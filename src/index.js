const changed = require("./hooks/changed");
const checkPermissions = require("./hooks/check-permissions");
const authentication = require("./services/authentication/authentication.service.js");
const users = require("./services/users/users.service.js");
const privileges = require("./services/privileges/privileges.service.js");
const organisations = require("./services/organisations/organisations.service.js");

module.exports = {
  hooks: { changed, checkPermissions },
  authentication,
  users,
  privileges,
  organisations
};
